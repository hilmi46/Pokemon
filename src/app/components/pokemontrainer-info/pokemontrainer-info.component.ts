import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokemontrainer-info',
  templateUrl: './pokemontrainer-info.component.html',
  styleUrls: ['./pokemontrainer-info.component.scss']
})
export class PokemontrainerInfoComponent implements OnInit {


  pokemonTrainer = {
    name: "Dewald",
    yearsOfTraining: 30,
    birthday: new Date(1942, 3, 22),
    favouritePokemons: ["charizard", "snorlex", "lugia"],
    image: "assets/Pikachu.png"
  }
  constructor() { }

  ngOnInit(): void {
    console.log("PokemontrainerInfoComponent.ngOnInit()");
      
  }
}
