import { Component, Input } from '@angular/core';
import { Pokemon } from 'src/app/modules/pokemon.model';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss']
})
export class PokemonsComponent {

  // HERE COMES TEXT
  // Define Inputs- Sent From the parent (page)
 @Input()
 title ="";
 @Input()
 showPokemons = true;

 @Input()
 pokemons: Pokemon[] = []

 constructor() {
   
 }
 ngOnInit() {

}


}

