import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/modules/user.model';
import { FavouriteService } from 'src/app/services/favourite.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-favourite-button',
  templateUrl: './favourite-button.component.html',
  styleUrls: ['./favourite-button.component.css']
})
export class FavouriteButtonComponent implements OnInit {

  public loading: boolean = false;
  public isFavourite: boolean = false;
  @Input() pokemonId: string = "";

  constructor(
    private userService: UserService,
    private readonly favouriteService: FavouriteService
  ) { }

  ngOnInit(): void {
    // Inputs are resolved!
    this.isFavourite = this.userService.inFavourites(this.pokemonId);
  }

  onFavouriteClick(): void {
    this.loading = true;
    // Add the pokemon to the favourites!
    this.favouriteService.addToFavourites(this.pokemonId)
      .subscribe({
        next: (user: User) => {
          this.loading = false;
          this.isFavourite = this.userService.inFavourites(this.pokemonId);
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message); 
        }
      })
  }

}
