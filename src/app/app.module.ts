import { NgModule } from '@angular/core';
import { HttpClientModule} from  "@angular/common/http";


import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonsComponent } from './components/pokemons/pokemons.component';
import { PokemontrainerPage } from './pages/pokemontrainer/pokemontrainer.page';
import { PokemontrainerInfoComponent } from './components/pokemontrainer-info/pokemontrainer-info.component';
import { PokemonsPage } from './pages/pokemons/pokemons.page';
import { LoginPage } from './pages/login/login.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FavouriteButtonComponent } from './components/favourite-button/favourite-button.component';


@NgModule({
  declarations: [
    AppComponent,
    PokemonsComponent,
    PokemontrainerPage,
    PokemontrainerInfoComponent,
    PokemonsPage,
    LoginPage,
    LoginFormComponent,
    PokemonListItemComponent,
    NavbarComponent,
    FavouriteButtonComponent,
  ],
  imports: [ // Angular Modules
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]

  
})
export class AppModule { }
