import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginPage } from './pages/login/login.page';
import { PokemonsPage } from './pages/pokemons/pokemons.page';
import { PokemontrainerPage } from './pages/pokemontrainer/pokemontrainer.page';
const routes: Routes = [ 
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login"
  },
  {
    path: "pokemontrainer",
    component: PokemontrainerPage,
    canActivate: [AuthGuard]

  },
  {
    path:"pokemons",
    component: PokemonsPage,
    canActivate: [AuthGuard]
  },

  {
    path:"login",
    component: LoginPage

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
