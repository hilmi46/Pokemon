import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap} from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../modules/user.model';

const {apiUsers, apiKey} = environment


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency Injection
  constructor(private readonly http: HttpClient) {}

   public login(username: string): Observable<User>{ 
     return this.checkUsername(username)
     .pipe(
       switchMap((user: User | undefined) => {
        if (user === undefined) { // If user does not exist
          return this.createUser(username)
        }
        return of(user);
       })
     )
   }

  // Check if user exists
 

  private checkUsername(username: string): Observable<User | undefined> {
    // This query parameter will check if the user exists and then it comes with an array or an empty one.
    // We need to sort that data with this
    
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
    .pipe(
      map((response: User[]) => response.pop())
      )   
   }
  // IF not user - create a User

   private createUser(username: string): Observable<User> {
     const user = {
       username,
       favourites: []
     }
     // create User
     const headers = new HttpHeaders({
       "Content-Type": "application/json",
       "x-api-key": apiKey
     })

     // We need Headers and API key
     // POST request -- create items on the server
    return this.http.post<User>(apiUsers, user, {
      headers
    } )

   }

  // If user || Created user - store User
}
