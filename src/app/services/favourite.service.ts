import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../modules/pokemon.model';
import { User } from '../modules/user.model';
import { PokemonsService } from './pokemons.service';
import { UserService } from './user.service';

const { apiKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root',
})
export class FavouriteService {
  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading
  }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonsService,
    private readonly userService: UserService
  ) {}
 
  public addToFavourites(pokemonId: string): Observable<User> {

    if (!this.userService.user) {
      throw new Error('addToFavourites: There is no user');
    }

    const user: User = this.userService.user;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId); //

    if (!pokemon) {
      throw new Error('AddToFavourites: No pokemon with id: ' + pokemonId);
    }

    if (this.userService.inFavourites(pokemonId)) {
      this.userService.removeFromFavourites(pokemonId);
    } else {
      this.userService.addToFavourites(pokemon);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });



    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
        favourites: [...user.favourites, pokemon]
        },{
        headers
      })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser;
        }),
        finalize(() => {
          this._loading = false;
        })
      )
  }
}
