import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Pokemon, PokemonResponse } from 'src/app/modules/pokemon.model';
import { environment } from 'src/environments/environment';
const { apiPokemons, apiPokemonImage} = environment



@Component({
  selector: 'app-pokemons-page', 
  templateUrl: './pokemons.page.html',
  styleUrls: ['./pokemons.page.scss'],
})

export class PokemonsPage implements OnInit{
  // This is how you display an Array in Angular. See pokemons.component.html file
  public pokemons: Pokemon[] = [];
  public  showPokemons: boolean = true;
  public pokedex: string = 'deactivate Pokedex';


  

  // Dependency Injection
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.fetchPokemon();
  }



  openClosePokedex() {
    this.showPokemons = !this.showPokemons;

    if (this.showPokemons === false) {
      this.pokedex = 'Activate Pokedex';
    } else {
      this.pokedex = 'Deactivate Pokedex';
    }
  }

    

  private getImageUrl(url: string): string {
    const id = Number(url.split( '/' ).filter(Boolean).pop());
    return `${apiPokemonImage}/${id}.png`;
      
  }

  private fetchPokemon(): void {
    //Observables.
    this.http.get<PokemonResponse>(apiPokemons) //<--- Generic
        .subscribe( {
              next: (response: PokemonResponse) => {
                this.pokemons = response.results.map(pokemon => {
                  return {
                    ...pokemon,
                    image: this.getImageUrl(pokemon.url)
                  }
                })
              },
              error: () => {},
              complete: () => {}
          });

  }
}
