import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pokemontrainer',
  templateUrl: './pokemontrainer.page.html',
  styleUrls: ['./pokemontrainer.page.scss']
})
export class PokemontrainerPage implements OnInit {

  constructor() { }

  ngOnInit(): void { // When HTML is mounted.
    console.log("PokemontrainerPage.ngOnInit()");
    
  }

}
