import { Component, OnInit } from '@angular/core';
import { PokemonsService } from './services/pokemons.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
  

})
export class AppComponent implements OnInit {
  constructor(
    private readonly userService: UserService,
    private readonly pokemonService: PokemonsService
  ) {}

  ngOnInit(): void {
    if (this.userService.user) {
      this.pokemonService.findAllPokemons();
    }
  }
}
