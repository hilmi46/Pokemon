export interface Pokemon {
    id: string;
    image: string;
    name: string;
    url: string;
}

export interface PokemonResponse {
  count: number;
  next: string;
  previous: string;
  results:Pokemon[]
}